import ip from 'ip';

const
  dev = (req, res) => {
    res.render(`dev`);
  },
  error = (err, req, res, next) => {
    res.status(500);
    res.render(`500`, {error: err, title: `500: Internal Server Error`});
  },
  index = (req, res) => {
    (process.env.NODE_ENV === `production`) ?
        res.render(`index`, {prod: true}) : res.render(`index`, {prod: false, addr: ip.address()});
  },
  mocks = (req, res) => {
    res.render(`mocks/${req.params.id}`);
  },
  notFound = (req, res) => {
    res.status(404);
    res.render(`404`, {title: `404: Resource not found`});
  };

export default {
  dev,
  error,
  index,
  mocks,
  notFound
};
