import bs from 'browser-sync';
import chokidarEmitter from 'chokidar-socket-emitter';
import compression from 'compression';
import express from 'express';
import favicon from 'serve-favicon';
import stylus from 'stylus';

import routes from './routes.js';

const
  app = express(),
  viewPath = `${__dirname}/app/views`;

app.set(`port`, (process.env.NODE_ENV === `production`) ? 3000 : 2368);
app.set(`views`, viewPath);
app.set(`view engine`, `pug`);
app.use(compression());
app.use(favicon(`${__dirname}/app/dist/assets/img/favicon/favicon.ico`));
app.use(stylus.middleware({
  src: `${__dirname}/app/styles`,
  dest: `${__dirname}/app/dist/css`,
  compress: true,
  compile: (str, path) => {
    return stylus(str)
        .set(`filename`, path)
        .set(`compress`, true);
  }
}));

app.get(`/`, routes.index);
app.get(`/dev`, routes.dev);
app.get(`/mocks/:id`, routes.mocks);

app.use(express.static(`${__dirname}/app/dist`));

app.use(routes.notFound);
app.use(routes.error);

app.listen(app.get(`port`), () => {
  if (process.env.NODE_ENV !== `production`) {
    if (process.env.NODE_ENV.indexOf(`front`) > -1) {
      chokidarEmitter({port: 5776});
    }
    bs({
      files: [`app/styles/*.styl`, `app/views/*.pug`, `app/views/mocks/*.pug`],
      open: false,
      proxy: `localhost:${app.get(`port`)}`
    });
    console.log(`listening on port ${app.get(`port`)} w/ hotreloading`);
  } else {
    console.log(`listening on port ${app.get(`port`)}`);
  }
});
