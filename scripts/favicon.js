#!/usr/bin/env node

const
  favicons = require('favicons'),
  fs = require('fs');

  faviconSrc = `app/assets/img/icon.png`,
  faviconCfg = {
    appName: `smh`,
    appDescription: `Health & Wellness Sickle Cell app`,
    developerName: `brwnrclse (Barry Harris)`,
    developerURL: `https://brwnrclse.xyz`,
    path: `/`,
    url: `brwnrclse.xyz/smh`,
    display: `standalone`,
    orientation: `portrait`,
    version: `1.0`,
    logging: false,
    online: false,
    icons: {
      android: true,
      appleIcon: true,
      appleStartup: true,
      coast: true,
      favicons: true,
      firefox: true,
      opengraph: true,
      twitter: true,
      windows: true,
      yandex: true
    }
  },
  faviconCb = (err, res) => {
    if (err) {
      console.log(JSON.stringify(error));
    }
    res.images.map((c, i, a) => {
      fs.writeFile(`./app/dist/assets/img/favicon/${c.name}`, c.contents);
    });
  };

favicons(faviconSrc, faviconCfg, faviconCb);
