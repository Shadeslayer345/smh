# Spare Me How
General Health and Wellness app for sickle cell patients


### Getting Started

The codebase lives inside of `app/`, from there you can modify or import necessary modules into the server.js file or modify routes (back-end) in the routes folder. Front-end code lives in side the `app/dist/js` folder. Here our app is broken into components (react views), stores (data handling) and utilities. ** Before beginning any front-end tasks, it is far easier to use the design workflow utilizing jade and stylus w/ browser-sync.


#### Design
For desiigners, simple clone the repo
``` bash
cd somewhere
git clone https://bitbucket.org/Shadeslayer345/smh

```

Install dependencies (you'll need nodejs and npm)

``` bash

cd smh
npm install

```

Then go ahead and run you development server!

```bash

npm run start:front:dev

```

Your views are located in `app/views` or `app/views/mocks` and accompanying styles in `app/styles`.
